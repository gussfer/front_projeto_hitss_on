import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Admin from "./pages/Admin";
import Home from "./pages/Home"
import NavBar from "./components/NavBar";
import styled from 'styled-components'
import {CoursePage} from "./pages/CoursePage"
import CreateLessonPage from "./pages/CreateLessonPage"
import LessonPage from "./pages/LessonPage"

const PageContainer = styled.div`
padding-top: 62px;
background-color: whitesmoke;
`;

function App(){
  return (
    <BrowserRouter>
    <NavBar/>
    <PageContainer>
      <Routes> {/*Listagem das rotas */}
        <Route path="/admin" element={<Admin/>} />
        <Route path="/" element={<Home/>}/>
        <Route path="/curso/:id_course" element={<CoursePage/>}/>
        <Route path="/nova-aula/:id_course" element={<CreateLessonPage/>}/> 
        <Route path="/aula/:id_course/:number" element={<LessonPage/>}/>
     </Routes>
     </PageContainer>
    </BrowserRouter>
  );
}
export default App;