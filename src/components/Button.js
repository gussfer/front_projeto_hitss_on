import styled from 'styled-components'

const StyledButton = styled.button`
    background-color: #39c8d3;
    transform: translate(600px, -120px);
    outline: none;
    border: none;
    color: rgb(139, 245, 69);
    cursor: pointer;
    font-size: 23px;
    padding: 21px 11px;
    border-radius: 15px;
    mix-blend-mode: hard-light;
    align-self: center;
    margin: 5px 0;
    transition: 0.2 ease all;
    &:hover {
    opacity: 0.7;
    }
`;


export default StyledButton;