import React from 'react';
import styled from 'styled-components';

const StyledCard = styled.button`

display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
color: #282c34;
background-color: whitesmoke;
cursor: pointer;
font-family: Source Sans Pro, sans-serif;
border-radius: 9px;
box-shadow: 0px 1px 20px 0px rgb(0 0 0 / 5%); 
margin: 5px;
opacity: ${props => props.disable ? '0.5' : '1.0'};
cursor: ${props => props.disable ? 'not-allowed' : 'pointer'};
.card-header {
    font-size: 22px;
    font-family: Source Sans Pro, sans-serif;
    margin: 10px;
}
.card-text{
    font-size: 18px;
    font-family: Source Sans Pro, sans-serif;
    margin: 10px;
    line-height:1.5;
}
`
const Card = (props) => {
    return (
        <StyledCard {...props} title={''}>
            <h2 className="card-header">{props.title}</h2>
            <p className="card-text">{props.body}</p>
        </StyledCard>
    );
}
export default Card;