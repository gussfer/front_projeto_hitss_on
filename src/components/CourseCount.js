import styled from 'styled-components'

export const CourseCont = styled.div`
    width: 80%;
    margin: auto;
    min-width: 300px;
    display: flex;
    flex-direction: row;
    align-items: stretch;
    justify-content: space-between;
    .listRow {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: flex-start;
        width: 60%;
        margin-right: 20px;
    }
    .header {
        font-size: 28px;
        font-weight: normal;
        margin: 12px 0;
        font-family: Source Sans Pro, sans-serif;
    }
    .description {
        width: 35%;
        margin-top: 24px;
        font-family: Source Sans Pro, sans-serif;
        
    }
    .title {
        font-size: 24px;
        font-weight: normal;
        margin: 14px 0;
        font-family: Source Sans Pro, sans-serif;
    }
    .info{
        font-size: 14px;
        font-weight: normal;
        font-family: Source Sans Pro, sans-serif;
        
    }
  `;