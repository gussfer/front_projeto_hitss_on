import styled from 'styled-components';

export const ModalBoxAdCourse = {
    content: {
      position: 'relative',
      width: '240px',
      height: '270px',
      inset: '50% auto auto 50%',
      border: '1px solid grey',
      background: 'whitesmoke',
      overflow: 'auto',
      outline: 'none',
      padding: '79px 93px 133px 101px',
      borderRadius: '9px',
      transform: 'translate(-50%, -50%)',
      
    }
  }
export const StyledInput = styled.input`
    type: text;
    margin: 5px;
    font-size: 18px;
    
`;
export const SendButton = styled.button`
    
    color: #282c34;
    background: none;
    border: double;
    border-radius: 7px;
    cursor: pointer;
    font-size: 18px;
`;
export const StyledPostDiv = styled.div`
display: flex;
flex-direction: column;
align-items: unset;



`;
  
