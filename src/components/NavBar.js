import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {MainContext} from '../contexts/MainContext';
import Modal from 'react-modal';
import ModalBox from './../components/modalBoxComponents/ModalBox.js';
import StyledH3 from './../components/modalBoxComponents/H3.js';
import StyledInput from './../components/modalBoxComponents/Input.js';
import {SendButton, ExitButton} from './../components/modalBoxComponents/sendButton.js'
import * as usersServices from './../services/Users'
import {useNavigate} from 'react-router-dom'

const StyledNav = styled.nav`

position: fixed;
display: flex;
width: 100vw;
min-height: 17px;
height: 17px;
z-index: 1000;
background: #293351;
padding: 2%;
font-family: Source Sans Pro, sans-serif;
box-shadow: 0px 8px 16px 0px rgb(0 0 0 / 2%);
justify-content: space-between;
.logo-header{
  text-decoration-line: none;
  justify-content: center;
  display: flex;
  flex-direction: column;
  color: #ffdf32;
  cursor: pointer;
  font-size: 30px;
}
`;
const Dropdown = styled.div`
  position: relative;
  display: inline-block;
  padding: 25px;
  padding-right: 60px;
  padding-top: 0px;
  font-size: 24px;
  cursor: pointer;
  .dropdown-content{
    display: none;
    position: absolute;
    right:45px;
    top:33px;
    font-size: 14px;
    background-color: #ffdf32;
    width: 120px;
    box-shadow: 0px 8px 16px 0px rgb(0 0 0 / 2%);
    padding: 12px 16px;
    z-index: 1;
  }
  &: hover .dropdown-content {
    display: block;
  }
`;
 const LoginButton = styled.button`
  background: none;
  outline: none;
  border: none;
  border-radius: 9px;
  color: #ffdf32;
  cursor: pointer;
  font-size: 22px;
  font-family: Source Sans Pro, sans-serif;
  padding: 6px 60px 31px 5px;
  display: flex;
  align-items: center;
 `
Modal.setAppElement('#root')
  
const NavBar = (props) => {
  const [isPopup, setIsPopup] = React.useState(false); 
  function handleOpenModal(){
    setIsPopup(true);
  }
  function handleCloseModal(){
    setIsPopup(false);
  }
  const [inputEmail, setInputEmail] = React.useState('')
  const [inputPassword, setInputPassword] = React.useState('')
  const  {userInfo, setUserInfo} = React.useContext(MainContext)
  const navigate = useNavigate()
  //Requisição Login
  const handleSubmit = (e) => {
    e.preventDefault();
  usersServices.login({email: inputEmail, password: inputPassword})// post login, alocado em ./../services/Users
    .then((response) => setUserInfo(response.data))
    .catch((err) => {console.error("Erro"+ err);
    });
  }

    return (
        <StyledNav {...props}>
          <Link className="logo-header" to="/"><b>Hitss ON</b></Link>
          {
            userInfo ? 
              <Dropdown className="dropdown">
                <span style={{color: "#ffdf32"}}><b>{userInfo.user.first_name}</b></span>
                <div className="dropdown-content">
                  <p onClick={() => navigate('/admin')} style={{cursor:"pointer"}}>Perfil</p>
                  <p onClick={() => navigate('/admin')} style={{cursor:"pointer"}}>Administrador</p>
                  <p  onClick={() => setUserInfo(null)} style={{cursor:"pointer"}}>Sair</p>
                </div>
              </Dropdown>                    
              : 
              <LoginButton onClick={handleOpenModal}><b>Login</b></LoginButton>
          }
            <Modal isOpen={isPopup} onRequestClose={handleCloseModal} style={ModalBox}>
            <StyledH3>Login</StyledH3>
          {
            userInfo && userInfo.user ?
            <>
              <h2>
                Bem vindo {userInfo.user.first_name}!
              </h2>
              <ExitButton style={{transform: 'translate(255px,-222px)'}}onClick={(e) => {setUserInfo(null)}}>
                <b>Exit</b>
              </ExitButton>
            </> 
            :
            <form>
              <StyledInput placeholder="E-mail" value={inputEmail} onChange={(e) => setInputEmail(e.target.value)}></StyledInput>
              <StyledInput placeholder="Senha" value={inputPassword} onChange={(e) => setInputPassword(e.target.value)}></StyledInput>
              <SendButton onClick={(e) => handleSubmit(e)}>Enviar</SendButton>
              <ExitButton onClick={handleCloseModal}><b>X</b></ExitButton>
            </form>
          }
            </Modal>
        </StyledNav>
    )
}
export default NavBar