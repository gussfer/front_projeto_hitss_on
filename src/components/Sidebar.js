import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.ul`
    position: absolute;
    top: -25px;
    left: 0;
    height: 100vh;
    z-index: 900;
    background-color: #d8dce6;
    box-shadow: 0px 8px 16px 0px rgb(0 0 0 / 2%);
    display:flex;
    padding-top: 60px;
    padding-left: 0px;
    flex-direction: column;
    justify-content: flex-start;
`
const StyledLi = styled.li`
    width: 200px;
    color: black;
    display:flex;
    justify-content: center;
    font-size: 22px;
    background-color: ${props => props.selectTab ? '#555555' : 'transparent'};
    padding: 5px;
    &: hover {
        background-color: #ffdf32 ;
        cursor: pointer;
        
    }
`;

const Sidebar = ({items, handleSelect, selectTab}) => {
    return (
        <StyledContainer className="sidebar">
            {
                
            items.map((item, index) => (
            <StyledLi selectTab={item.id === selectTab} onClick={() => handleSelect(item.id)} key={`${item.id}-item`}>
                    {item.label}
            </StyledLi>
            ))
            }
        </StyledContainer>
    );
}
export default Sidebar;