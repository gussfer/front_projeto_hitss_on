import React from 'react';
import styled from 'styled-components';
import useVideoPlayer from '../utils/Hooks/useVideoPlayer'
import {updateProgress} from "./../services/Courses"

export const PlayerContainer = styled.div`
video {
    width: 100%;
  }
  
  .video-wrapper {
    width: 100%;
    max-width: 700px;
    position: relative;
    display: flex;
    justify-content: center;
    overflow: hidden;
    border-radius: 6px;
    top: 10px
  }
  
  .video-wrapper:hover .controls {
    transform: translateY(0%);
  }
  
  .controls {
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    position: absolute;
    bottom: 0px;
    margin: 20px;
    width: 100%;
    max-width: 580px;
    flex-wrap: wrap;
    background: rgba(255, 255, 255, 0.25);
    box-shadow: 0 8px 32px 0 rgba(255, 255, 255, 0.1);
    backdrop-filter: blur(4px);
    -webkit-backdrop-filter: blur(4px);
    border-radius: 10px;
    border: 1px solid rgba(255, 255, 255, 0.18);
    transform: translateY(150%);
    transition: all 0.3s ease-in-out;
  }
  
  .actions button {
    background: none;
    border: none;
    outline: none;
    cursor: pointer;
  }
  
  .actions button i {
    background-color: none;
    color: white;
    font-size: 30px;
  }
  
  input[type="range"] {
    -webkit-appearance: none !important;
    background: rgba(255, 255, 255, 0.2);
    border-radius: 20px;
    height: 4px;
    width: 350px;
  }
  
  input[type="range"]::-webkit-slider-thumb {
    -webkit-appearance: none !important;
    cursor: pointer;
    height: 6px;
  }
  
  input[type="range"]::-moz-range-progress {
    background: white;
  }
  
  .velocity {
    appearance: none;
    background: none;
    outline: none;
    border: none;
    text-align: center;
  }
  
  .mute-btn {
    background: none;
    border: none;
    outline: none;
    cursor: pointer;
  }
  
  .mute-btn i {
    background-color: none;
    color: white;
  }
`;

const VideoPlayer = (props) => {
    const videoElement = React.useRef(null);
    const wrapper = React.useRef(null);
    const [isWatched, setWatched] = React.useState(false)
    const {
        playerState,
        togglePlay,
        handleOnTimeUpdate,
        handleVideoProgress,
        handleVideoSpeed,
        toggleMute,
    } = useVideoPlayer(videoElement);
    const toggleFullscreen = () => {
      if (document.fullscreenElement) {
          document.exitFullscreen()
      }
      if (wrapper.current.requestFullScreen) {
        wrapper.current.requestFullScreen();
      } else if (wrapper.current.msRequestFullScreen) {
          wrapper.current.msRequestFullScreen();
      } else if (wrapper.current.mozRequestFullScreen) {
          wrapper.current.mozRequestFullScreen();
      } else if (wrapper.current.webkitRequestFullScreen) {
          wrapper.current.webkitRequestFullScreen();
      }
  };

  React.useEffect(() => {
    if (playerState.progress > 10 && isWatched === false) {
      console.log("Assistido")
      setWatched(true)
      updateProgress(props.course_id, props.user_id, props.lessonNumber).then(() => {props.hadleWatched()})
      
    }
// eslint-disable-next-line react-hooks/exhaustive-deps
}, [playerState.progress, isWatched]);

    return (
        <PlayerContainer>
            <div className="video-wrapper"
                ref={wrapper}>
                <video 
                       src={props.video_path}
                       ref={videoElement}
                       onTimeUpdate={handleOnTimeUpdate}
                       onClick={togglePlay}
                    />
                <div className="controls">
                    <div className="actions">
                        <button onClick={togglePlay}>
                        {
                            !playerState.isPlaying ? (<p>Play</p>) : (<p>Pause</p>)
                        }
                        </button>
                    </div>
                    <input 
                        type='range'
                        min="0"
                        max="100"
                        value={playerState.progress}
                        onChange={(e) => handleVideoProgress(e)}/>
                    <select
                        className="velocity"
                        value={playerState.speed}
                        onChange={(e) => handleVideoSpeed(e)}>
                        <option value="0.50">0.50x</option>
                        <option value="1">1x</option>
                        <option value="1.25">1.25x</option>
                        <option value="2">2x</option>
                    </select>
                    <button className="mute-btn" onClick={toggleMute}>
                        {
                        !playerState.isMuted ? (<p>Mute</p>) : (<p>Unmute</p>)  
                        }
                    </button>
                    <button className="mute-btn" onClick={toggleFullscreen}>
                      Full
                    </button>
                </div>
            </div>
        </PlayerContainer>
    )
}
export default VideoPlayer;