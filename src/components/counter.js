import React from 'react';
import CC from './modalBoxComponents/CC.js';

//Componente funcional
const Counter = (props) => {
    const [count, setCount] = React.useState(props.initial) // hooks: associados a componente funcional
    
   
    React.useEffect(() => { // hook effect
        document.title = `Hitss ON`
    })
    
    return (
        <div className="counter">
            <CC>
                {count}
            </CC>
            <button onClick={()=> setCount(count+1)}>Incremento</button>
            <button onClick={()=> setCount(count-1)}>Decremento</button>
        </div>
    );
}
export default Counter;

//Componente de classe 
// class Counter extends React.Component{//extends React.Component {
//     constructor(props) { // constructor define os estados
//         super(props); // permite alterar os estados
//         this.state = {count: props.initial};
//     }
//  render() {
//     return(
//         <div className="counter">
//             <h3>
//                 {this.state.count}
//             </h3>
//             <button onClick={()=> this.setState(prevState => ({count: prevState.count + 1}))}>Adicionar</button>
//         </div>
//      );
//     }
// }
