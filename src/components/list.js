import React from 'react';
import styled from 'styled-components';

const StyledLi = styled.li`
    width: 200px;
    display:flex;
    justify-content: space-between;
`
const StyledBtn = styled.button`
    color: rgb(139, 245, 69);
    background-color: transparent;
    border: transparent;
    font-size: 1.5rem;
`

const List = ({initial}) => {
    const [list, setList] = React.useState(['item teste']) // hooks: associados a componente funcional
    const [currentValue, setValue] = React.useState('') // hooks sintaxe: "const [nomeDoEstado, nomeParaFunção]"
    const handleSubmit = () => {
        setList([...list, currentValue])
        setValue('')
    }
    React.useEffect(() => {
        console.log("Lista atualizada")
        
    }, [list]) //passar estado alvo em um cenário com mais de um estado
    React.useEffect(() => {
        console.log("Atualizando lista")
        
    }, [currentValue]) //passar estado alvo em um cenário com mais de um estado
    return (
        <>
            <input type="text" value={currentValue} style={{margin: '4px'}} onChange={(e) => setValue(e.target.value)}/>
            <button id = "Adicionar" onClick={() => handleSubmit()}>Adicionar</button>
            <ul className="list">
                {
                    list.map((item, index) => (
                    <StyledLi key={`${index}-item`}>
                            {item}
                        <StyledBtn onClick={() => setList(list.filter((value)=> value !== list[index]))}>x</StyledBtn>
                    </StyledLi>
                    ))
                }
            </ul>
        </>
    );
}
// Clicar no botão usando a tecla enter
    //obs: necessário adicionar "id" ao button
document.addEventListener("keypress", function(e) {
    if (e.key === "Enter") {
        const btn = document.querySelector("#Adicionar");
        btn.click();
    }
})
export default List;