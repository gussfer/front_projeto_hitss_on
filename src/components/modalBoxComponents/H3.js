import styled from 'styled-components';

  const StyledH3 = styled.h3 `
  display: flex;
  text-align: center; 
  color: #282c34;
  justify-content: center;
  outline: none;
  transform: translate(1px,-45px);
  position: relative;
  width:239px;
  height: 27px;
  font-size: 2.3em;
  font: Source Sans Pro, sans-serif;
  `
  export default StyledH3