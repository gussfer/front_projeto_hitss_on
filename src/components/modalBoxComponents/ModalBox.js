export const ModalBox = {
    content: {
      position: 'relative',
      width: '240px',
      height: '270px',
      inset: '50% auto auto 50%',
      border: 'double',
      background: 'whitesmoke',
      overflow: 'auto',
      outline: 'none',
      padding: '79px 93px 133px 101px',
      borderRadius: '9px',
      transform: 'translate(-50%, -50%)'
    }
  }
  export default ModalBox;