import styled from 'styled-components';

const SendButton = styled.button `
transform: translate(86px,0px);
color: #282c34;
background: none;
border: double;
border-radius: 7px;
cursor: pointer;
font-size: 18px;
`
const ExitButton = styled.button `
transform: translate(225px,-248px);
color: #282c34;
background-color: transparent;
border: none;
cursor: pointer;
font-size: 1.6em;
font-family: cursive;
`

export  {SendButton, ExitButton}