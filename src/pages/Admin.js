import React from "react";
import {useParams} from "react-router-dom";
import Sidebar from "../components/Sidebar";
import {getCoursesList, createCourse, editCourse, deleteCourse} from "./../services/Courses";
import styled from "styled-components";
import Modal from 'react-modal';
import {ModalBoxAdCourse, StyledInput, SendButton, StyledPostDiv} from "../components/ModalAdCourse"
import {ModalEditCourse, StyledEditDiv, StyledEditInput} from '../components/ModalEditCourse'
import {createUser, getUsersList, deleteUser, editUser} from "./../services/Users"
const Panel = styled.div`
    display: ${props => props.active ? 'flex' : 'none'};
    box-sizing: border-box;
    width: 100%;
    padding: 10px 30% 0px calc(145px + 5%);
    flex-direction: column;
    justify-content: space-between;
    font-size: 20px;
    color: #333;
`;
const StyledLi = styled.li`
    width: 80%;
    margin: 20px;
    border-radius: 4px;
    width: 122%;
    background-color: whitesmoke;
    margin-left: 0px;
    min-width: 300px;
    display: flex;
    justify-content: space-between;
    height: 30px;
    `;
const ButtonAd = styled.button`
    width: 80%;
    margin: 20px;
    border-radius: 4px;
    width: 122%;
    background-color: whitesmoke;
    margin-left: 0px;
    min-width: 300px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    cursor: pointer;
    font-family: cursive;
    border: 1px solid grey;
`;
Modal.setAppElement('#root')

const reducerForm = (prev, e) => {
    return{...prev, [e.target.name]: e.target.value}
}

function Admin(){
    //Modal Create Course
    const [isPopup, setIsPopup] = React.useState(false); 
    function handleOpenModal(){
    setIsPopup(true);
    }
    function handleCloseModal(){
    setIsPopup(false);
    }
    //Modal Edit Course
    const [isPopupEdit, setIsPopupEdit] = React.useState(false); 
    function handleOpenModalEdit(){
    setIsPopupEdit(true);
    }
    function handleCloseModalEdit(){
    setIsPopupEdit(false);
    }
    //Modal Delete Course
    const [isPopupDelete, setIsPopupDelete] = React.useState(false); 
    function handleOpenModalDelete(){
    setIsPopupDelete(true);
    }
    function handleCloseModalDelete(){
    setIsPopupDelete(false);
    }
    //Modal Create Aluno
    const [openCreateAluno, setCreateAluno] = React.useState(false); 
    function handleOpenCreateAluno(){
        setCreateAluno(true);
    }
    function handleCloseCreateAluno(){
        setCreateAluno(false);
    }
    //Modal Edit Aluno
    const [isPopupEditAluno, setIsPopupEditAluno] = React.useState(false); 
    function handleOpenModalEditAluno(){
    setIsPopupEditAluno(true);
    }
    function handleCloseModalEditAluno(){
    setIsPopupEditAluno(false);
    }
    //Modal Delete Aluno
    const [isPopupDeleteAluno, setIsPopupDeleteAluno] = React.useState(false); 
    function handleOpenModalDeleteAluno(){
    setIsPopupDeleteAluno(true);
    }
    function handleCloseModalDeleteAluno(){
    setIsPopupDeleteAluno(false);
    }
    
    const [activeTab, setActiveTab] = React.useState(0);
    const sideBarItems = [
        {
            label: 'Gerenciar Cursos',
            id: 0
        },
        {
            label: 'Gerenciar Alunos',
            id: 1
        },
        {
            label: 'Perfil',
            id: 2
        }
    ]
    const [coursesList, setCoursesList] = React.useState([])
    React.useEffect(() => {
        getCourses()
    },[])
    const getCourses = () => {
        getCoursesList().then((response) => {
            setCoursesList(response.data)
        })
    };

    //Requisição Post Course
    const [inputTitulo, setInputTitulo] = React.useState('')
    const [inputDescricao, setInputDescricao] = React.useState('')
    
    const handleSubmit = (e) => {
        e.preventDefault();
        createCourse({Title: inputTitulo, Course_Resume: inputDescricao})// post login, alocado em ./../services/Users
        .then((response) => getCourses())
        handleCloseModal()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    //Requisição Post Aluno
    const [usersList, setUsersList] = React.useState([])
    React.useEffect(() => {
        getUsers()
    }, [])
    const getUsers = () => {
        getUsersList().then((response) => {
            setUsersList(response.data)
        })
    };
    
    const userData = {
        first_name: '',
        last_name: '',
        email: '',
        password: '',
      }
    const [formAluno, setForm] = React.useReducer(reducerForm, userData)
    const handleCreateAluno = (e) => {
        e.preventDefault();
        createUser(formAluno)// post login, alocado em ./../services/Users
        .then((response) => getUsers())
        handleCloseCreateAluno()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    //Requisição Edit Course
    const handleEdit = (item) => { //retorna dados ja existentes no bd
        setputTitulo(item.Title)
        setputDescricao(item.Course_Resume)
        setEdit(item.id_course)
        handleOpenModalEdit()
    }
    const [putTitulo, setputTitulo] = React.useState('')
    const [putDescricao, setputDescricao] = React.useState('')
    const [courseEdited, setEdit] = React.useState('')
    const handleSubmitEdit = (e) => {
        e.preventDefault();
        editCourse(courseEdited,{Title: putTitulo, Course_Resume: putDescricao})
        .then((response) => getCourses())
        handleCloseModalEdit()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    //Requisição Edit Aluno
    const handleEditAluno = (item) => { //retorna dados ja existentes no bd
        setputName(item.first_name)
        setputSobre(item.last_name)
        setputEmail(item.email)
        setputPass(item.password)
        setEditUser(item.id_users)
        handleOpenModalEditAluno()
    }
    const [putName, setputName] = React.useState('')
    const [putSobre, setputSobre] = React.useState('')
    const [putEmail, setputEmail] = React.useState('')
    const [putPass, setputPass] = React.useState('')
    const [userEdited, setEditUser] = React.useState('')
    const handleSubmitEditAluno = (e) => {
        e.preventDefault();
        editUser(userEdited,{
            first_name: putName, 
            last_name: putSobre, 
            email: putEmail,
            password: putPass})
        .then((response) => getUsers())
        handleCloseModalEditAluno()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    
    //Requisição Delete Course
    const handleDelete = (e, id) => {
        e.preventDefault();
        deleteCourse(id)
        .then((response) => getCourses())
        handleCloseModalDelete()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    //Requisição Delete Aluno
    const handleDeleteAluno = (e, id) => {
        e.preventDefault();
        deleteUser(id)
        .then((response) => getUsers())
        handleCloseModalDeleteAluno()
        .catch((err) => {console.error("Erro"+ err);
        });
    }
    
    return (
        <div>
            <Sidebar items={sideBarItems} handleSelect={(value) => setActiveTab(value)}/>
            <Panel active={activeTab === 0}>
                <ul>
                <h3>Gerenciar Cursos</h3>
                    {
                        coursesList && coursesList.map((item, index) => (
                        <StyledLi key={`${item.id_course}-item`}>{item.Title}
                            <span>
                                <button onClick={() => handleEdit(item)}>Edit</button>
                                <Modal isOpen={isPopupEdit} onRequestClose={handleCloseModalEdit} style={ModalEditCourse}>
                                <StyledEditDiv>
                                    <h1>Edit Curso</h1>
                                    <StyledEditInput placeholder="Título" value={putTitulo} onChange={(e) => setputTitulo(e.target.value)}></StyledEditInput>
                                    <StyledEditInput placeholder="Descrição" value={putDescricao} onChange={(e) => setputDescricao(e.target.value)}></StyledEditInput>
                                    <SendButton onClick={(e) => handleSubmitEdit(e)}>Enviar</SendButton> 
                                </StyledEditDiv>
                                </Modal>
                                <button onClick={handleOpenModalDelete}>x</button>
                                <Modal isOpen={isPopupDelete} onRequestClose={handleCloseModalDelete} style={ModalEditCourse}>
                                    <h3>Deseja excluir este curso?</h3>
                                    <div style={{display: 'grid'}}>
                                        <button onClick={(e)=> handleDelete(e, item.id_course)}>Sim</button>
                                        <button onClick={handleCloseModalDelete}>Não</button> 
                                    </div>                                   
                                </Modal>
                            </span>
                        </StyledLi>
                        ))
                    }
                <ButtonAd onClick={handleOpenModal}>+ Adicionar curso</ButtonAd>
                <Modal isOpen={isPopup} onRequestClose={handleCloseModal} style={ModalBoxAdCourse}>
                    <StyledPostDiv>
                        <h1>Novo Curso</h1>
                        <StyledInput placeholder="Título" value={inputTitulo} onChange={(e) => setInputTitulo(e.target.value)}></StyledInput>
                        <StyledInput placeholder="Descrição" value={inputDescricao} onChange={(e) => setInputDescricao(e.target.value)}></StyledInput>
                        <SendButton onClick={(e) => handleSubmit(e)}>Criar Curso</SendButton>
                    </StyledPostDiv>
                </Modal>
                </ul>
            </Panel>
            <Panel active={activeTab === 1}>
                <ul>
                    <h3>Gerenciar Alunos</h3>
                        {
                            usersList && usersList.map((item) => (
                            <StyledLi key={`${item.id_users}-item`}>{item.first_name + " " + item.last_name}
                            <span>
                            <button onClick={() => handleEditAluno(item)}>Edit</button>
                                <Modal isOpen={isPopupEditAluno} onRequestClose={handleCloseModalEditAluno} style={ModalEditCourse}>
                                <StyledEditDiv>
                                    <h1>Edit Aluno</h1>
                                    <StyledEditInput placeholder="Nome" value={putName} onChange={(e) => setputName(e.target.value)}></StyledEditInput>
                                    <StyledEditInput placeholder="Sobrenome" value={putSobre} onChange={(e) => setputSobre(e.target.value)}></StyledEditInput>
                                    <StyledEditInput placeholder="Email" value={putEmail} onChange={(e) => setputEmail(e.target.value)}></StyledEditInput>
                                    <StyledEditInput placeholder="Password" value={putPass} onChange={(e) => setputPass(e.target.value)}></StyledEditInput>
                                    <SendButton onClick={(e) => handleSubmitEditAluno(e)}>Enviar</SendButton> 
                                </StyledEditDiv>
                                </Modal>
                                <button onClick={handleOpenModalDeleteAluno}>x</button>
                                <Modal isOpen={isPopupDeleteAluno} onRequestClose={handleCloseModalDeleteAluno} style={ModalEditCourse}>
                                    <h3>Deseja excluir este Aluno?</h3>
                                    <div style={{display: 'grid'}}>
                                        <button onClick={(e)=> handleDeleteAluno(e, item.id_users)}>Sim</button>
                                        <button onClick={handleCloseModalDeleteAluno}>Não</button> 
                                    </div>                                   
                                </Modal>
                            </span>
                            </StyledLi>
                            ))
                        }
                    <ButtonAd onClick={handleOpenCreateAluno}>+ Adicionar aluno</ButtonAd>
                    <Modal isOpen={openCreateAluno} onRequestClose={handleCloseCreateAluno} style={ModalBoxAdCourse}>
                        <StyledPostDiv>
                            <h1>Novo Aluno</h1>
                            <StyledInput placeholder="Nome" value={formAluno.first_name} name="first_name" onChange={(e) => setForm(e)}></StyledInput>
                            <StyledInput placeholder="Sobrenome" value={formAluno.last_name} name="last_name" onChange={(e) => setForm(e)}></StyledInput>
                            <StyledInput placeholder="Email" value={formAluno.email} name="email" onChange={(e) => setForm(e)}></StyledInput>
                            <StyledInput placeholder="Senha" value={formAluno.password} name="password" onChange={(e) => setForm(e)}></StyledInput>
                            <SendButton onClick={(e) => handleCreateAluno(e)}>Criar Aluno</SendButton>
                        </StyledPostDiv>
                    </Modal>
                </ul>
            </Panel>
            <Panel active={activeTab === 2}>
                <h3 className="title">{sideBarItems[activeTab].label}</h3>
            </Panel>
        </div>
    )
}
export default Admin;