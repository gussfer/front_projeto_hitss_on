import React from "react";
import Card from "./../components/Card";
import {getCoursesLessons, getCourse, getCertificate, checkProgress} from "./../services/Courses";
import {CourseCont} from "./../components/CourseCount"
import {useNavigate} from "react-router-dom";
import {useParams} from "react-router-dom"
import {MainContext} from '../contexts/MainContext';

export function CoursePage(){
  const [lessonsList, setLessons] = React.useState([])
  const [courseInfo, setCourse] = React.useState([])  
  const [lastSeen, setLastSeen] = React.useState(0)
  const {isAdmin, userInfo} = React.useContext(MainContext)
  
  const params = useParams()
  const navigate = useNavigate()
  const getInfo = async (id_course, user_id) => {
      const lessons = await getCoursesLessons(id_course)
      const course = await getCourse(id_course)
      const progress = await checkProgress(id_course, user_id)
      setLessons(lessons.data)
      setCourse(course.data[0])
      setLastSeen(progress.data.lastSeen)
  }
  React.useEffect(() => {
      getInfo(params.id_course, userInfo.user.id_users)
  }, [params.id_course, userInfo.user.id_users]);

    return (
    <div>
    <CourseCont>
        <div className="listRow">
            <h2 className="header"><b>{courseInfo.Title}</b></h2>
            {
                isAdmin && <button onClick={() => navigate(`/nova-aula/${courseInfo.id_course}`)} 
                                   style={{margin: "5px"}}> + Adicionar nova aula</button>}
            {
            lessonsList && lessonsList.map((item) => (
                <Card key={item.id_lesson} 
                      disable={lastSeen + 1 < item.number} 
                      onClick={() => {
                        if (lastSeen + 1 >= item.number) 
                        {
                            navigate(`/aula/${courseInfo.id_course}/${item.number}`)
                        }}}
                      title={<><b>Aula {item.number}: </b>{item.title}</>} 
                      body={item.lesson_resume}/>
                ))}
        </div>
        <div className="description">
            <h2 className="title">Resumo</h2>
            <p className="info">{courseInfo.Course_Resume}</p>
            <button onClick={() => getCertificate(params.id_course, userInfo.user.id_users)}>
                Download do certificado
            </button>
        </div>
    </CourseCont>
    </div>
    
    )
}
 