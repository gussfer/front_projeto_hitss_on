import React from "react";
import Card from "./../components/Card";
import {getCoursesLessons, createLesson, uploadVideo} from "./../services/Courses";
import styled from "styled-components";
import {useNavigate} from "react-router-dom";
import {useParams} from "react-router-dom"

const CourseCont = styled.div`
    width: 80%;
    margin: auto;
    min-width: 300px;
    display: flex;
    flex-direction: row;
    align-items: stretch;
    justify-content: space-between;
    .listRow {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: flex-start;
        width: 60%;
        margin-right: 20px;
    }
    .header {
        font-size: 28px;
        font-weight: normal;
        margin: 12px 0;
        font-family: sans-serif;
        line-height:1.5;
    }
    .description {
        width: 35%;
        margin-top: 24px;
        font-family: sans-serif;
    }
    .title {
        font-size: 28px;
        font-weight: normal;
        margin: 14px 0;
    }
    .info{
        font-size: 18px;
        font-weight: normal;
        
    }
  `;
const reducerForm = (prev, e) => {
    if (e.action === 'substitute'){
        return e.value
    }
    return{...prev, [e.target.name]: e.target.value}
}
function CreateLessonPage(){
  const [lessonsList, setLessons] = React.useState([])
  const [step, setStep] = React.useState(0)  
  const [videoFile, setFile] = React.useState(null) 
  const initial = {
    title: '',
    lesson_resume: '',
    number: 0,
  }
  const [form, setForm] = React.useReducer(reducerForm, initial)
  const params = useParams()
  const navigate = useNavigate()
  const getInfo = async (id_course) => {
      const lessons = await getCoursesLessons(id_course)
      setLessons(lessons.data)
      setForm({target: {name: 'number', value: lessons.data.length + 1}})
  }
  React.useEffect(() => {
      getInfo(params.id_course)
  }, [params])

  const handleSubmit = (e) => {
    e.preventDefault()
    createLesson({...form, id_course: params.id_course})
    setStep(1)
    .catch(err => {
        console.error("Erro"+ err);
    });
    setForm({action: 'substitute', value: {title:'', lesson_resume: ''}})
}
const handleUpload = async (e) => {
    e.preventDefault();
    if (videoFile === null) {
        alert("Não rolou")
        return null
    }
    const upload = await uploadVideo(videoFile, params.id_course, form.number)
    console.log(upload.data)
    navigate(`/curso/${params.id_course}`)
}
const getStep = () => {
    switch (step) {
        case 0:
           return (
           <div className="listRow">
               <h1> Criar aula</h1>
               <input style={{margin: '4px'}} type="text" placeholder="Título" name="title" value={form.title} onChange={(e) => setForm(e)}/>
               <input style={{margin: '4px'}} type="text" placeholder="Descrição" name="lesson_resume" value={form.lesson_resume} onChange={(e) => setForm(e)}/>
               <label htmlFor="number">Nº da aula</label>
               <input style={{margin: '4px'}} type="number" placeholder="Número da aula" name="number" value={form.number} onChange={(e) => setForm(e)}/>
               <button style={{margin: '4px'}} onClick={(e) => handleSubmit(e)}>Enviar</button>

           </div>
           )
           default:
            return (
                <div className="listRow">
                    <h1>Upload de video</h1>
                    <form>
                        <h5 className="info"><b>Título da aula: </b>{form.title}</h5>
                        <h5 className="info"><b>Descrição: </b>{form.lesson_resume}</h5>
                        <h5 className="info"><b>Vídeo:</b></h5>
                        <input id="file"
                               type="file"
                               onChange={(e) => setFile(e.target.files[0])}/>
                        <button onClick={(e) => handleUpload(e)}>Enviar</button>
                    </form>
                </div>
            )   
    }
}
  
return (
<CourseCont>
    {getStep()}
</CourseCont>
    
    
    )
}
export default CreateLessonPage 