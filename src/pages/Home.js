import React from "react";
import Card from "./../components/Card";
import {getCoursesList} from "./../services/Courses";
import styled from "styled-components";
import {useNavigate} from "react-router-dom";

const ListRow = styled.div`
    width: 80%;
    margin: 30px;
    margin-left: 40px;
    min-width: 300px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    .title {
        color: #282c34;
        font-size: 28px;
        font-family: Source Sans Pro, sans-serif;
    }
  `;

function Home(){
  const [coursesList, setCoursesList] = React.useState([])  
  const navigate = useNavigate()
  React.useEffect(() => {
      getCoursesList().then((response) => {
          setCoursesList(response.data)
      })
  }, []);
  
    return (
    <div>
        <ListRow>
            <h2 className="title">Cursos Disponíveis</h2>
        {
            coursesList && coursesList.map((item) => (
                <Card key={item.id_course + "-course"} 
                onClick={() => navigate (`/curso/${item.id_course}`)}
                title={<><span>Curso: </span>{item.Title}</>}
                body={item.Course_Resume}
                />
                ))
        }
        </ListRow>
    </div>
    )
}
export default Home