/* eslint-disable no-sequences */
import React from "react";
import Card from "./../components/Card";
import {getLesson, getVideo, checkProgress} from "./../services/Courses";
import styled from "styled-components";
import {useNavigate} from "react-router-dom";
import {useParams} from "react-router-dom"
import {MainContext} from '../contexts/MainContext';
import VideoPlayer from "../components/VideoPlayer";

const CourseCont = styled.div`
    width: 80%;
    margin: auto;
    min-width: 300px;
    display: flex;
    flex-direction: row;
    align-items: stretch;
    justify-content: space-between;
    font-family: Source Sans Pro, sans-serif;
    .videoRow {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: flex-start;
        width: 60%;
        margin-right: 20px;
    }
    .header {
        font-size: 28px;
        font-weight: normal;
        margin: 12px 0;
        font-family: Source Sans Pro, sans-serif;
        line-height:1.5;
    }
    .description {
        width: 35%;
        margin-top: 10px;
        font-family: Source Sans Pro, sans-serif;
    }
    .title {
        font-size: 20px;
        font-weight: normal;
        margin: 14px 0;
        font-family: Source Sans Pro, sans-serif;
        
    }
    .info{
        font-size: 14px;
        font-weight: normal;
        font-family: Source Sans Pro, sans-serif;
        
    }
  `;

function LessonPage(){
  const [lessonInfo, setLesson] = React.useState({})
  const [nextLessonInfo, setNextLesson] = React.useState({})
  const [video_path, setvideo] = React.useState('')  
  const {userInfo} = React.useContext(MainContext) 
  const [lastSeen, setLastSeen] = React.useState(0)
  const params = useParams()
  const navigate = useNavigate()

  const getInfo = async (id_course, number) => {
      const currentLessons = await getLesson(id_course, number)
      const nextLesson = await getLesson(id_course, Number(number)+1)
      const video = await getVideo(id_course, number)
      const progress = await checkProgress(id_course, userInfo.user.id_users)
      setLesson(currentLessons.data)
      setvideo(video.data)
      setNextLesson(nextLesson.data)
      setLastSeen(progress.data.lastSeen)
  }
  React.useEffect(() => {
      getInfo(params.id_course, params.number)
  }, [params.id_course, params.number]);
  
    return (
    <div>
        <CourseCont>
            <div className="videoRow">
                {video_path !== '' && (
                    <VideoPlayer  video_path={video_path} 
                    hadleWatched={() => checkProgress(params.id_course, userInfo.user.id_users).then((resp) =>setLastSeen(resp.data.lastSeen))}
                    course_id={params.id_course} 
                    user_id={userInfo.user.id_users} 
                    lessonNumber={params.number}
                    />
                )}
                <h2 className="header">Aula {lessonInfo.number} - {lessonInfo.title}</h2>
            </div>
            <div className="description"style={{display: "flex", flexDirection: "column"}}>
                <h2 className="title"><b>Sobre a trilha</b></h2>
                <p className="info">{lessonInfo.lesson_resume}</p> 
                {
                    nextLessonInfo && <Card style={{backgroundColor: "#293351", color: "#ffdf32"}} title = {"Próxima aula"} disable={lastSeen + 1 < nextLessonInfo.number} body={nextLessonInfo.title} onClick={() => navigate(`/aula/${lessonInfo.id_course}/${nextLessonInfo.number}`)}/>
                }
            </div>
        </CourseCont>
    </div>
    
    )
}
export default LessonPage 