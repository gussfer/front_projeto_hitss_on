import api from './api'
import fileDownload from 'js-file-download'

const getCoursesLessons = async (id_course) => {
    try { 
      return await api.get(`courses/list/${id_course}`)
    } catch (error) {
        console.log('Erro na busca de aulas desse curso', error)
        return error
    }
}
const getLesson = async (id_course, lessonNumber) => {
  try {
    return await api.get(`/courses/video/${id_course}/${lessonNumber}`)
  } catch (error) {
    console.log('Erro na busca de aulas desse curso', error)
        return error
  }
}
const getVideo = async (id_course, lessonNumber) => {
  try {
    return await api.get(`/course/${id_course}/${lessonNumber}`)
  } catch (error) {
    console.log('Erro na busca de aulas desse curso', error)
        return error
  }
}
const getCourse = async (id_course) => {
  try {
    return await api.get(`course/${id_course}`)
  } catch (error) {
    console.log('Erro na busca de aulas desse curso', error)
        return error
  }
}
const getCoursesList = async () => {
  try { 
    return await api.get(`courses`)
  } catch (error) {
      console.log('Erro na busca de aulas desse curso', error)
      return error
  }
}
const createCourse = async ({Title, Course_Resume}) => {
  try {
    return await api.post(`course`, {Title, Course_Resume})
  } catch (error) {
    console.log('Erro ao criar curso', error)
    return error
  }
}
const uploadVideo = async (body, id_course, lessonNumber) => {
  let formData = new FormData();
  formData.append("video", body);
  try {
    return await api.post(`courses/upload/${id_course}/${lessonNumber}`, formData); 
  } catch (error) {
    console.log('Erro no upload', error)
    return error
  }
  console.log("Teste 1")
}

const createLesson = async (form) => {
  try {
    return await api.post(`course/video`, form)
  } catch (error) {
    console.log('Erro ao criar curso', error)
    return error
  }
}

const editCourse = async (id_course, body) => {
  try {
    return await api.put(`course/${id_course}`, body)
  } catch (error) {
    console.log('Erro na busca de aulas desse curso', error)
        return error
  }
}
const deleteCourse = async (id_course) => {
  try {
    return await api.delete(`course/${id_course}`)
  } catch (error) {
    console.log('Erro na busca de aulas desse curso', error)
        return error
  }
}
const getCertificate = async (id_course, id_users) => {
  try { 
    return await api.get(`courses/pdf/${id_course}/${id_users}`, {responseType: 'blob'}).then((response) => {
      fileDownload(response.data, 'certificado.pdf')
    })
  } catch (error) {
      console.log('Erro na geração do certificado', error)
      return error
  }
}

const checkProgress = async (course_id, user_id) => {
  try {
    return await api.get(`courses/progress/${course_id}/${user_id}`)
  } catch (error) {
    console.log('Erro no progresso', error)
    return error
  }
}
const updateProgress = async (course_id, user_id, lastSeen) => {
  try {
    return await api.post(`courses/progress/${course_id}/${user_id}`, {lastSeen: lastSeen})
  } catch (error) {
    console.log('Erro no progresso', error)
    return error
  }
}

export {getCoursesLessons, getCoursesList, getCourse, editCourse, createCourse, 
  deleteCourse, createLesson, uploadVideo, getLesson, getVideo, getCertificate, 
  checkProgress, updateProgress}
