import api from './api'

const login = ({email,password}) => {
    try {
        return api.post("user/login", {email, password})
    } catch (error) {
        console.log("Erro no login: ", error)
        return error
    }
}
const getUsersList = async () => {
    try { 
      return await api.get(`users`)
    } catch (error) {
        console.log('Erro na busca de aulas desse curso', error)
        return error
    }
  }
const createUser = (userData) => {
    try {
        return api.post("user", userData)
    } catch (error) {
        console.log("Erro na criação do aluno: ", error)
        return error
    }
}
const deleteUser = async (id) => {
    try {
      return await api.delete(`user/${id}`)
    } catch (error) {
      console.log('Erro na busca de aulas desse curso', error)
          return error
    }
  }
  const editUser = async (id, userData) => {
    try {
      return await api.put(`user/${id}`, userData)
    } catch (error) {
      console.log('Erro na busca de aulas desse curso', error)
          return error
    }
  }
export {login, createUser, getUsersList, deleteUser, editUser}